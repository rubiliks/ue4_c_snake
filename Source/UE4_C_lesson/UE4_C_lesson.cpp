// Copyright Epic Games, Inc. All Rights Reserved.

#include "UE4_C_lesson.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UE4_C_lesson, "UE4_C_lesson" );
