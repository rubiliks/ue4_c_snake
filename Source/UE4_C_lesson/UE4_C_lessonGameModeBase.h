// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE4_C_lessonGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE4_C_LESSON_API AUE4_C_lessonGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
